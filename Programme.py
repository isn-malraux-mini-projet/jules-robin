import os
from PIL import Image
image = ''
img_chemin = ''

def clearscreen():
    os.system("clear")
def ouvrirImage():
    """Fonction qui ouvre l'image demandée par l'utilisateur
    
    Usage:
    >>>Donner le chemin d'accès au fichier :
    Image.jpg
    ouvre l'image
    
    Conditions initiales : Aucune
    Argument: aucun
    Sortie: aucun
    """
    global image
    global img_chemin
    img_chemin = str(input("Chemin d'accès relatif ou absolu au fichier :"))
    image = Image.open(img_chemin)
    image.convert("RGB")
    image.show()
    clearscreen()
    Menu()
    
def afficherImage():    
    """Fonction qui ouvre une image chargée.
    Usage
    >>>chargerImageargerImage()
    ouvre l'image
    
    Conditions initiales: Aucune
    Argument: Aucun
    Sortie: Aucun
    """
    global image
    image.show()
    clearscreen()
    Menu()
    
def infoImage():
    """Fonction qui donne les informations d'une image.
    
    Usage
    >>>infoImage()
    propriétés de Image.jpeg:
        mode colorimétrique: RGB
        fomat de l'image: Jpeg
        dimensions: 1920*1080
        
    Arguments: 
    Conditions :
    Sortie: 
    """
    global image
    global img_chemin
    mode = image.mode
    forme = image.format
    largeur,hauteur = image.size
    print("Propriétés de l'image %s: " %(img_chemin))
    print("    mode colorimétrique: %s" %(mode))
    print("    taille de l'image: %sx%s" %(largeur, hauteur))    
    print("    format de l'image: %s" %(forme))
    quitter= str(input("Pour quitter, appuyer sur une touche : " ))
    clearscreen()
    Menu()

def negatifCouleur():
    clearscreen()
    (hauteur, largeur)= image.size
    imagenega = Image.new("RGB",(hauteur, largeur))
    for ligne in range (hauteur):
        for colonne in range (largeur):
            Rouge, Vert, Bleu= image.getpixel((ligne,colonne))
            (Rouge,Vert,Bleu) = (255-Rouge,255-Vert,255-Bleu)
            imagenega.putpixel((ligne,colonne), (Rouge, Vert, Bleu))
    imagenega.show()
    clearscreen()
    Menu()
    
def monochrome():
    img2= image.convert('L')
    img2.show()
    clearscreen()
    Menu()
    
def transfoSymetrique():
    clearscreen()
    (largeur, hauteur) = image.size
    print("Comment voulez vous retourner l'image ?")
    print("- H - Retourner horizontalement")
    print("- V - Retourner verticalement")
    print("Entrer un caractere pour revenir au menu")
    choice=str(input("Choix: "))
    if choice == "H":
        symetrieHorizontal()
    elif choice == "V":
        symetrieVertical()
    else:
        clearscreen()
        Menu()
def symetrieHorizontal():
    (largeur, hauteur) = image.size
    imageHorizontal = Image.new("RGB",(largeur, hauteur))
    imageHorizontal = image.transpose(Image.FLIP_TOP_BOTTOM)
    imageHorizontal.show()
    transfoSymetrique()
def symetrieVertical():
    (largeur, hauteur) = image.size
    imageVertical = Image.new("RGB",(largeur, hauteur))
    imageVertical = image.transpose(Image.FLIP_LEFT_RIGHT)
    imageVertical.show()
    transfoSymetrique()
    
def rotationCouleur():
    largeur,hauteur = image.size
    clearscreen()
    print("Quelle rotation de couleur voulez vous ?")
    print("    -RGB")
    print("    -BRG")
    print("    -GBR")
    print("    -BGR")
    print("Pour revenir au menu, entrez un caractère,")
    Rotation = str(input("Entrez le type de rotation : "))
    if Rotation ==  ("RGB") :
        rotationRGB()
            
    elif Rotation == ("BRG") :
        rotationBRG()
        
    elif Rotation == ("GBR") :
        rotationGBR()                    

    elif Rotation == ("BGR") :
        rotationBGR()
        
    else :
        clearscreen()
        Menu()
        
def rotationRGB():
    global image
    image.show()
    clearscreen()
    rotationCouleur()

def rotationBRG():
    global image
    largeur,hauteur = image.size
    imageBRG = Image.new('RGB',(largeur,hauteur))
    for ligne in range(largeur):
        for colonne in range(hauteur):
            Rouge,Vert,Bleu= image.getpixel((ligne,colonne))
            imageBRG.putpixel((ligne,colonne),(Vert,Bleu,Rouge))        
    imageBRG.show()
    rotationCouleur()
            
def rotationGBR():
    global image
    largeur,hauteur = image.size
    imageGBR = Image.new('RGB',(largeur,hauteur))
    for ligne in range(largeur):
        for colonne in range(hauteur):
            Rouge,Vert,Bleu=image.getpixel((ligne,colonne))
            imageGBR.putpixel((ligne,colonne),(Bleu,Rouge,Vert))
    imageGBR.show()
    rotationCouleur()
    
def rotationBGR():
    global image
    largeur,hauteur = image.size
    imageBGR = Image.new('RGB',(largeur,hauteur))
    for ligne in range(largeur):
        for colonne in range(hauteur):
            Rouge,Vert,Bleu=image.getpixel((ligne,colonne))
            imageBGR.putpixel((ligne,colonne),(Bleu,Vert,Rouge))
    imageBGR.show()
    rotationCouleur()
    
def rotateImage():
    """Fonction qui fait pivoter une image selon le choix de l'utilisateur.
    
    usage
    >>>
    
    """
    clearscreen()
    print("Dans quel sens doit pivoter l'image ?")
    print("    -gauche")
    print("    -droite")
    print("    -retourner")
    print("Pour retourner au menu, entrer un caractère.")
    angle = str(input("Choix : "))
    if angle == "gauche":
        image.rotate(270).show()
        rotateImage()
    elif angle == "droite":
        image.rotate(90).show()
        rotateImage()
    elif angle == "retourner":
        image.rotate(180).show()
        rotateImage()
    else:
        clearscreen()
        Menu()
    
def Menu():
    
    valeur=0
    while valeur != "Q":
        print("Que voulez-vous faire ?")
        print("- O - Ouvrir une image et l'afficher") #done
        print("- A - Afficher l'image chargée (la charger au besoin)") #done
        print("- I - Afficher les informations sur l'image") #done
        print("- N - Mettre l'image en négatif") #done
        print("- M - Passer l'image en noir et blanc") #done
        print("- C - Effectuer la rotation des couleurs de l'image") #presque done
        print("- R - Effectuer la rotation physique de l'image") #Pas fait
        print("- S - Effectuer une transformation symétrique de l'image") #Pas fait
        print("- Q - Quitter le programme") #done

        valeur=str(input("Choix : "))
        if valeur == "O" or valeur=="o":
            ouvrirImage()
        elif valeur == "A" or valeur=="a":
            afficherImage()
        elif valeur == "I" or valeur=="i":
            clearscreen()
            infoImage()
        elif valeur == "N" or valeur=="n":
            negatifCouleur()
        elif valeur == "M" or valeur == "m":
            monochrome()
        elif valeur == "C" or valeur=="c":
            rotationCouleur()
        elif valeur == "R" or valeur=="r":
            rotateImage()
        elif valeur == "S" or valeur=="s":
            transfoSymetrique()
        elif valeur == "Q" or valeur=="q":
            print("Au revoir !")
            quit
        else:
            print("Vous avez mal entré votre valeur, veuillez réessayer")
Menu()
