    (largeur, hauteur) = image.size
    print("Comment voulez vous retourner l'image ?")
    print("- H - Retourner horizontalement")
    print("- V - Retourner verticalement")
    print("- Q - Retourner au menu")
    def symetrieHorizontal():
        imageHorizontal = Image.new("RGB",(largeur, hauteur))
        imageHorizontal = image.transpose(Image.FLIP_TOP_BOTTOM)
        imageHorizontal.show()
        transfoSymetrique()
    def symetrieVertical():
        imageVertical = Image.new("RGB",(largeur, hauteur))
        imageVertical = image.transpose(Image.FLIP_LEFT_RIGHT)
        imageVertical.show()
        transfoSymetrique()
    choice=str(input("Choix:"))
    if choice == "H" or "h":
        symetrieHorizontal()
    elif choice == "V" or choice == "v":
        symetrieVertical()
    else:
        clearscreen()
        Menu()