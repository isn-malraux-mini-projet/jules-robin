#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 29 11:34:22 2017

@author: utilisateur
"""
import os
from PIL import Image
image = ''
img_chemin = ''
def quitter():
    
def clearscreen():
    os.system("cls")
def ouvrirImage():
    """Fonction qui ouvre l'image demandée par l'utilisateur
    
    Usage:
    >>>Donner le chemin d'accès au fichier :
    Image.jpg
    ouvre l'image
    
    Conditions initiales : Aucune
    Argument: aucun
    Sortie: aucun
    """
    global image
    global img_chemin
    img_chemin = str(input("Chemin d'accès relatif ou absolu au fichier :"))
    image = Image.open(img_chemin)
    image.show()
def afficherImage():    
    """Fonction qui ouvre une image chargée.
    Usage
    >>>chargerImageargerImage()
    ouvre l'image
    
    Conditions initiales: Aucune
    Argument: Aucun
    Sortie: Aucun
    """
    global image
    image.show()
def infoImage():
    """Fonction qui donne les informations d'une image.
    
    Usage
    >>>infoImage()
    propriétés de Image.jpeg:
        mode colorimétrique: RGB
        fomat de l'image: Jpeg
        dimensions: 1920*1080
        
    Arguments: 
    Conditions :
    Sortie: 
    """
    global image
    global img_chemin
    mode = image.mode
    forme = image.format
    largeur,hauteur = image.size
    print("Propriétés de l'image %s: " %(img_chemin))
    print("    mode colorimétrique: %s" %(mode))
    print("    taille de l'image: %sx%s" %(largeur, hauteur))    
    print("    format de l'image: %s" %(forme))
    

print("Que voulez-vous faire ?")
print("- O - Ouvrir une image et l'afficher")
print("- A - Afficher l'image chargée (la charger au besoin)")
print("- I - Afficher les informations sur l'image")
print("- N - Mettre l'image en négatif")
print("- C - Effectuer la rotation des couleurs de l'image")
print("- R - Effectuer la rotation physique de l'image")
print("- S - Effectuer une transformation symétrique de l'image")
print("- Q - Quitter le programme")

valeur=0
while valeur != "Q":
    valeur=str(input("Choix : "))
    if valeur == "O" or valeur=="o":
        ouvrirImage()
    elif valeur == "A" or valeur=="a":
        afficherImage()
    elif valeur == "I" or valeur=="i":
        clearscreen()
        infoImage()
    elif valeur == "N" or valeur=="n":
        pass
    elif valeur == "C" or valeur=="c":
        pass
    elif valeur == "R" or valeur=="r":
        pass
    elif valeur == "S" or valeur=="s":
        pass
    elif valeur == "Q" or valeur=="q":
        print("Au revoir !")
        exit
    else:
        print("Vous avez mal entré votre valeur, veuillez réessayer")

